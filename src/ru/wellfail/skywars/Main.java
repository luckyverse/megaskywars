package ru.wellfail.skywars;

import org.bukkit.plugin.java.JavaPlugin;
import ru.wellfail.skywars.data.shop.ShopLoader;
import ru.wellfail.skywars.game.GameFactory;
import ru.wellfail.wapi.game.GameSettings;
import ru.wellfail.wapi.utils.Utils;

import java.util.List;

/**
 * Created by WellFail on 02.06.2015.
 */
public class Main extends JavaPlugin {

    private static Main instance;
    private List<String> board;

    public static Main getInstance() {
        return instance;
    }

    public void onLoad() {
        instance = this;
    }

    public List<String> getBoard() {
        return this.board;
    }

    public void onEnable() {
        this.saveDefaultConfig();

        board = loadBoard();
        System.out.println(board.size());
        setup();
        new ShopLoader();
        new GameFactory();
    }

    public List<String> loadBoard() {
        return getConfig().getStringList("Board");
    }

    public void setup() {
        GameSettings.animation = board;
        GameSettings.prefix = getConfig().getString("Prefix");
        GameSettings.wboardname = board.get(0);
        GameSettings.damage = true;
        GameSettings.isSpectate = true;
        GameSettings.damage = true;
        GameSettings.pickup = true;
        GameSettings.shop = true;
        GameSettings.inventory = true;
        GameSettings.drop = true;
        GameSettings.fallDamage = true;
        GameSettings.physical = true;
        GameSettings.itemSpawn = true;
        GameSettings.chest = true;
        GameSettings.isFoodChange = true;
        GameSettings.itemdrop = true;
        GameSettings.explode = true;
        GameSettings.party = true;
        GameSettings.partyLimit = 2;
        GameSettings.gameTime = -1;
        if (getConfig().getBoolean("Setup")) {
            GameSettings.setup = true;
        }
        GameSettings.slots = getConfig().getInt("Slots");
        GameSettings.toStart = GameSettings.slots - 4;
        GameSettings.mapLocation = Utils.stringToLocation(getConfig().getString("Map"));
    }

}
