package ru.wellfail.skywars.game;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.scheduler.BukkitRunnable;
import ru.wellfail.skywars.Main;
import ru.wellfail.skywars.data.shop.SouldGrade;
import ru.wellfail.wapi.events.DeathEvent;
import ru.wellfail.wapi.events.GenChestEvent;
import ru.wellfail.wapi.events.LoadShopEvent;
import ru.wellfail.wapi.game.GameSettings;
import ru.wellfail.wapi.game.Gamer;
import ru.wellfail.wapi.perks.PerkType;
import ru.wellfail.wapi.utils.Utils;

/**
 * Created by WellFail on 02.06.2015.
 */
public class GameListener implements Listener {




    @EventHandler
    public void onChestGen(GenChestEvent e) {
        if (e.getChest().getType() == Material.TRAPPED_CHEST) {
            Utils.stringToItems(e.getChest().getInventory(), Main.getInstance().getConfig().getString("Traped"), true, false);
        } else {
            Utils.stringToItems(e.getChest().getInventory(), Main.getInstance().getConfig().getString("Chest"), true, false);
        }
    }
    public static boolean damage = false;
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
        if (!(e.getEntity() instanceof Player)) return;
        Player damager = null;
        Player target = (Player) e.getEntity();
        if (e.getDamager() instanceof Player) {
            damager = (Player) e.getDamager();
        }
        if (e.getDamager() instanceof Projectile) {
            ProjectileSource shooter = ((Projectile) e.getDamager()).getShooter();
            if (shooter != null && shooter instanceof Player) {
                damager = (Player) shooter;
            }
        }
        if (damager == null || target == null) return;

        if(damage){
            return;
        }
        if(Gamer.getGamer(target).getPerk() == PerkType.DEFFER){
            e.setCancelled(true);
        }

    }

    @EventHandler
    public void onDeah(DeathEvent event) {
        if (event.getKill() != null) {
            Player killer = event.getKill();
            Gamer gamer = Gamer.getGamer(killer);
            if(!event.isFb()) {
                gamer.addMoney(40);
                gamer.getPlayer().sendMessage(GameSettings.prefix+"§7Вы получили §e40 §7монеток за первое убийство.");
            }else{
                gamer.addMoney(20);
                gamer.getPlayer().sendMessage(GameSettings.prefix+"§7Вы получили §e20 §7монеток за убийство.");
            }
            if (GameFactory.players.get(killer.getName()).getSoulGrade() == SouldGrade.COLLECTOR) {
                if (gamer.getSouls() >= 130) {
                    return;
                }
            } else {
                if (gamer.getSouls() >= 100) {
                    return;
                }
            }
            if (GameFactory.players.get(killer.getName()).getSoulGrade() == SouldGrade.KILLER) {
                gamer.setSouls(gamer.getSouls() + 2);
            } else {
                gamer.setSouls(gamer.getSouls() + 1);
            }
        }
    }


}
