package ru.wellfail.skywars.game;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import ru.wellfail.skywars.data.Config;
import ru.wellfail.skywars.data.GPlayer;
import ru.wellfail.skywars.Main;
import ru.wellfail.skywars.data.Stats;
import ru.wellfail.skywars.game.boards.GameBoard;
import ru.wellfail.skywars.game.boards.SpectatorBoard;
import ru.wellfail.skywars.game.stage.StageSwitcher;
import ru.wellfail.wapi.board.Board;
import ru.wellfail.wapi.game.Game;
import ru.wellfail.wapi.game.GameSettings;
import ru.wellfail.wapi.game.Gamer;
import ru.wellfail.wapi.gui.Item;
import ru.wellfail.wapi.gui.ItemRunnable;
import ru.wellfail.wapi.listeners.DamageListener;
import ru.wellfail.wapi.listeners.PhysicalListener;
import ru.wellfail.wapi.logger.WLogger;
import ru.wellfail.wapi.perks.PerkType;
import ru.wellfail.wapi.shop.Kit;
import ru.wellfail.wapi.shop.ShopManager;
import ru.wellfail.wapi.utils.FIterator;
import ru.wellfail.wapi.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Created by WellFail on 02.06.2015.
 */
public class GameFactory extends Game {

    public static Map<String, GPlayer> players;

    private Config config;
    private Stats stats;
    private static StageSwitcher stageSwitcher;

    public static StageSwitcher getStageSwitcher() {
        return stageSwitcher;
    }

    public GameFactory() {
        super("SWT", new GameListener());
        players = new ConcurrentHashMap();
        config = new Config();
        stats = new Stats();

        stageSwitcher = new StageSwitcher(this);
    }


    public static int getPlayers() {
        return players.size();
    }

    @Override
    public void onRespawn(Player player) {
    }

    @Override
    protected void onStartGame() {

        final DamageListener dmg = new DamageListener();
        final PhysicalListener phys = new PhysicalListener();

        new BukkitRunnable() {
            public void run() {
                HandlerList.unregisterAll(dmg);
                HandlerList.unregisterAll(phys);
            }
        }.runTaskLater(Main.getInstance(), config.getDelayColb() + 60);
        for (Player p : Bukkit.getOnlinePlayers()) {
            Location loc = config.getSpawns().getNext();
            p.teleport(loc);
            if (!loc.getChunk().isLoaded()) {
                loc.getChunk().load();
            }
            Utils.removeBlocks(p.getLocation());
            GPlayer pl = players.get(p.getName());
            if (pl.getCage() != null) {
                Utils.buildCell(p, pl.getCage());
            } else {
                Utils.buildCell(p, new ItemStack(Material.GLASS));
            }
            sendStartMessage(p);
            GameSettings.isBreak = false;
            GameSettings.isPlace = false;

            new BukkitRunnable() {
                public void run() {
                    Utils.removeBlocks(p.getLocation());
                    GameSettings.isBreak = true;
                    GameSettings.isPlace = true;
                    stageSwitcher.startSwitcher();
                    new BukkitRunnable() {
                        public void run() {
                            GameListener.damage = true;
                        }
                    }.runTaskLater(Main.getInstance(), 160);
                    Bukkit.getOnlinePlayers().stream().filter(player -> Gamer.getGamer(player).getPerk() == PerkType.POLTR).forEach(player -> {
                        player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 1000, 0));
                    });
                }
            }.runTaskLater(Main.getInstance(), config.getDelayColb());

            giveItems(p);
        }

    }

    public void giveItems(Player p) {
        if (Gamer.getGamer(p).getPurchase() != null) {
            for (ItemStack item : Gamer.getGamer(p).getPurchase().getItems()) {
                if (item == null) {
                    continue;
                }
                p.getInventory().addItem(item);
            }
        }
        new GameBoard(p);
    }

    private void sendStartMessage(Player p) {
        p.sendMessage("§a§l▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
        p.sendMessage("");
        p.sendMessage("                                   §c§lSkyWars");
        p.sendMessage("");
        p.sendMessage("                      §eПостарайся уничтожить всех, кроме себя.");
        p.sendMessage("                      §eВсе необходимое ты найдешь в сундуке...");
        p.sendMessage("");
        p.sendMessage("§a§l▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
    }


    @Override
    protected void onEndGame() {
        Player lastAlive = Utils.getLastAlive();
        stats.sava(Gamer.getGamers(), lastAlive);
        Bukkit.broadcastMessage("§a§l▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage("                               §c§lSky Wars");
        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage("                         §6Победил игрок - " + lastAlive.getDisplayName());
        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage("§a§l▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬");
        Gamer.getGamer(lastAlive).addMoney(50);
    }

}
