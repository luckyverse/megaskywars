package ru.wellfail.skywars.game.stage.dragon;


import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.EnderDragon;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;
import ru.wellfail.skywars.game.stage.dragon.utils.Util;
import ru.wellfail.skywars.game.stage.dragon.utils.UtilAlg;
import ru.wellfail.wapi.game.Gamer;


/**
 * Created by WellFail on 24.02.2016.
 */

public class Dragon {

    private EnderDragon dragon;
    public Location Location;
    private double Mult = 1.0D;
    public Location Target = null;
    private Vector Velocity;
    private float Pitch = 0.0F;


    public void setMult(double i)
    {
        this.Mult = i;
    }

    public void spawn(Location loc)
    {

        this.dragon = (EnderDragon)loc.getWorld().spawn(loc, EnderDragon.class);
        this.dragon.setCustomName("§eDoom stage");

        this.Velocity = dragon.getLocation().getDirection().setY(0).normalize();
        this.Pitch = UtilAlg.GetPitch(dragon.getLocation().getDirection());
        this.Location = dragon.getLocation();

    }

    public EnderDragon getDragon()
    {
        return dragon;
    }
    public void setTarget(Location loc)
    {
        this.Target = loc;
    }

    public void Move()
    {
        Turn();
        double speed = 0.2D;
        speed *= this.Mult;
        this.Location.add(this.Velocity.clone().multiply(speed));
        this.Location.add(0.0D, -this.Pitch, 0.0D);
        this.Location.setPitch(-1.0F * this.Pitch);
        this.Location.setYaw(180.0F + UtilAlg.GetYaw(this.Velocity));
        this.dragon.teleport(this.Location);
        Bukkit.getOnlinePlayers().stream().filter(player -> Util.getBlocksInRadius2D(this.Location, player.getLocation(), 16)).forEach(player -> {
            if(!Gamer.getGamer(player).isSpectator() && Updater.waypoints.contains(player)) {
                player.damage(100);
                Updater.nextPlayer();
            }
        });
    }

    private void Turn()
    {
        float desiredPitch = UtilAlg.GetPitch(UtilAlg.getTrajectory(this.Location, this.Target));
        if (desiredPitch < this.Pitch) {
            this.Pitch = ((float)(this.Pitch - 0.05D));
        }
        if (desiredPitch > this.Pitch) {
            this.Pitch = ((float)(this.Pitch + 0.05D));
        }
        if (this.Pitch > 0.5D) {
            this.Pitch = 0.5F;
        }
        if (this.Pitch < -0.5D) {
            this.Pitch = -0.5F;
        }
        Vector desired = UtilAlg.getTrajectory2d(this.Location, this.Target);
        desired.subtract(UtilAlg.Normalize(new Vector(this.Velocity.getX(), 0.0D, this.Velocity.getZ())));
        desired.multiply(0.2D);

        this.Velocity.add(desired);


        UtilAlg.Normalize(this.Velocity);
    }


}
