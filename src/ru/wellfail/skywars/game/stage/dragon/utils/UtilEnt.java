package ru.wellfail.skywars.game.stage.dragon.utils;

import org.bukkit.block.BlockFace;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;


/**
 * Created by WellFail on 07.04.2015.
 */
public class UtilEnt {

    public static boolean isGrounded(org.bukkit.entity.Entity ent)
    {
        if ((ent instanceof CraftEntity)) {
            return ((CraftEntity)ent).getHandle().onGround;
        }
        return UtilBlock.solid(ent.getLocation().getBlock().getRelative(BlockFace.DOWN));
    }
}
