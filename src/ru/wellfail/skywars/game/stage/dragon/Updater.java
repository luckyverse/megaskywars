package ru.wellfail.skywars.game.stage.dragon;


import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import ru.wellfail.skywars.Main;
import ru.wellfail.skywars.game.stage.dragon.utils.UtilBlock;
import ru.wellfail.skywars.game.stage.dragon.utils.UtilMath;

import java.util.Iterator;
import java.util.List;

/**
 * Created by WellFail on 08.04.2015.
 */
public class Updater {

    private static Dragon dragon;
    public static List<Player> waypoints;

    private static Player currentTarget;

    private static boolean alive = true;
    private static Iterator<Player> it;

    public Updater(Dragon dragon, List<Player> points) {
        this.dragon = dragon;
        this.waypoints = points;
        it = waypoints.iterator();
        nextPlayer();
    }

    public void start(Location loc) {
        new BukkitRunnable() {
            public void run() {
                loc.getChunk().load();
                dragon.spawn(loc);
            }
        }.runTask(Main.getInstance());


        this.dragon.setMult(0.0D);
        new BukkitRunnable() {
            @Override
            public void run() {
                if (!alive) {
                    dragon.setMult(1.0D);
                    this.cancel();
                }
                update();
            }
        }.runTaskTimer(Main.getInstance(), 0L, 1L);
        new BukkitRunnable() {
            @Override
            public void run() {
                if (!alive) {
                    dragon.setMult(1.0D);
                    this.cancel();
                }
            }
        }.runTaskTimer(Main.getInstance(), 0L, 20L);

    }

    public static void nextPlayer() {
        if (it.hasNext()) {
            currentTarget = it.next();
        } else {
            alive = false;
            dragon.getDragon().damage(900000);
            currentTarget = null;
        }
    }

    public void update() {
        if (currentTarget != null) {
            this.dragon.setTarget(currentTarget.getLocation());
            this.dragon.Move();
        }

    }
}
