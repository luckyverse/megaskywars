package ru.wellfail.skywars.game.stage;

/**
 * Created by WellFail on 12.02.2016.
 */
public interface StageRunnable {

    void stageStart();
}
