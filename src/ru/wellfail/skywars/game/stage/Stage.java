package ru.wellfail.skywars.game.stage;

import ru.wellfail.skywars.game.GameFactory;

/**
 * Created by WellFail on 12.02.2016.
 */
public abstract class Stage implements StageRunnable {

    private String name;
    private GameFactory factory;
    public int time;

    public int getTime(){
        return this.time;
    }

    public String getName() {
        return this.name;
    }

    public GameFactory getFactory(){
        return this.factory;
    }


    public Stage(String stageName, GameFactory factory, int time) {
        this.name = stageName;
        this.factory = factory;
        this.time = time;
    }

    public void start(){
        stageStart();
    }
}
