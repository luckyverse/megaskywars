package ru.wellfail.skywars.game.stage.stages;

import net.minecraft.server.v1_8_R3.EntityEnderDragon;
import org.bukkit.entity.Player;
import ru.wellfail.skywars.game.GameFactory;
import ru.wellfail.skywars.game.stage.Stage;
import ru.wellfail.skywars.game.stage.dragon.Dragon;
import ru.wellfail.skywars.game.stage.dragon.Updater;
import ru.wellfail.wapi.game.GameSettings;
import ru.wellfail.wapi.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by WellFail on 22.02.2016.
 */
public class EndDoom extends Stage {

    public EndDoom(String stageName, GameFactory factory, int time) {
        super(stageName, factory,time);
    }


    public void stageStart() {
        List<Player> players = new ArrayList<>();
        players.addAll(Utils.getAlivePlayers());
        Collections.shuffle(players);
        players.remove(new Random().nextInt(players.size()));
        Updater upd = new Updater(new Dragon(),players);
        upd.start(GameSettings.spectrLocation);

    }
}
