package ru.wellfail.skywars.game.stage.stages;

import ru.wellfail.skywars.game.GameFactory;
import ru.wellfail.skywars.game.stage.Stage;

/**
 * Created by WellFail on 12.02.2016.
 */
public class RefillStage extends Stage {

    public RefillStage(String stageName, GameFactory factory, int time) {
        super(stageName,factory,time);
    }

    public void stageStart() {
        getFactory().refillChests();
    }
}
