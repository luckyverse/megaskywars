package ru.wellfail.skywars.game.stage.stages;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import ru.wellfail.skywars.Main;
import ru.wellfail.skywars.game.GameFactory;
import ru.wellfail.skywars.game.stage.Stage;
import ru.wellfail.skywars.game.stage.dragon.Dragon;
import ru.wellfail.skywars.game.stage.dragon.Updater;
import ru.wellfail.wapi.game.GameSettings;
import ru.wellfail.wapi.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by WellFail on 12.02.2016.
 */
public class DoomStage extends Stage {

    public DoomStage(String stageName, GameFactory factory,int time) {
        super(stageName, factory,time);
    }

    public void stageStart() {
        List<Player> players = new ArrayList<>();
        players.addAll(Utils.getAlivePlayers());
        Collections.shuffle(players);
        for(int i = players.size()/2; i< players.size();i++){
            players.remove(i);
        }
        Updater upd = new Updater(new Dragon(),players);
        new BukkitRunnable(){
            @Override
            public void run() {
                upd.start(GameSettings.spectrLocation);
            }
        }.runTask(Main.getInstance());
    }
}
