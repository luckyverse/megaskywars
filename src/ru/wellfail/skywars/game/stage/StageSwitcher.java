package ru.wellfail.skywars.game.stage;

import ru.wellfail.skywars.game.GameFactory;
import ru.wellfail.skywars.game.stage.stages.DoomStage;
import ru.wellfail.skywars.game.stage.stages.EndDoom;
import ru.wellfail.skywars.game.stage.stages.RefillStage;
import ru.wellfail.wapi.game.GameState;
import ru.wellfail.wapi.utils.FIterator;
import ru.wellfail.wapi.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by WellFail on 12.02.2016.
 */
public class StageSwitcher {

    private FIterator<Stage> stages;

    private GameFactory factory;

    private Stage current;
    private int currSec = 0;

    public Stage getCurrentStage() {
        return this.current;
    }

    public int getCurrSec() {
        return this.currSec;
    }

    public String getTimeToEnd() {
        return Utils.getTimeToEnd(current.getTime() * 60 - currSec);
    }

    public StageSwitcher(GameFactory factory) {
        this.factory = factory;
        loadStages();

    }

    public boolean start = false;

    public void startSwitcher() {

        if(start){
            return;
        }
        start = true;

        new Thread(() -> {
            while (GameState.current == GameState.GAME) {
                try {
                    Thread.sleep(1000);
                    currSec++;
                    if (currSec >= current.getTime() * 60) {
                        current.start();
                        current = stages.getNext();
                        currSec = 0;
                    }
                } catch (Exception ignore) {
                    ignore.printStackTrace();
                }
            }
        }).start();
    }

    private void loadStages() {
        List<Stage> st = new ArrayList<>();
        //  st.add(new DoomStage("DOOM", factory, 1));
        st.add(new RefillStage("Обновление сундуков", factory, 3));
        st.add(new RefillStage("Обновление сундуков", factory, 5));
        st.add(new RefillStage("Обновление сундуков", factory, 5));
        //st.add(new EndDoom("DOOM", factory, 1));
        this.stages = new FIterator<>(st);
        this.current = this.stages.getNext();
    }


}
