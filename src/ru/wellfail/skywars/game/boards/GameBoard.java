package ru.wellfail.skywars.game.boards;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import ru.wellfail.skywars.Main;
import ru.wellfail.skywars.game.GameFactory;
import ru.wellfail.skywars.game.stage.StageSwitcher;
import ru.wellfail.wapi.board.Board;
import ru.wellfail.wapi.board.BoardLine;
import ru.wellfail.wapi.game.Gamer;
import ru.wellfail.wapi.utils.Utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by WellFail on 02.06.2015.
 */
public class GameBoard extends Board {

    private int d;
    private Player player;

    public GameBoard(Player player) {
        this.player = player;
        Set<BoardLine> lines = getLines();
        list = Main.getInstance().getBoard();
        create(lines, list.get(0));
        update(() -> {

            if (d == list.size())
                d = 0;
            setDisplay(list.get(d));
            d++;
        }, 3L);
        set(player);
        updateHealth(player);
        startUpdate();
    }

    private List<String> list;
    private BoardLine players, spectators, kills, stage;


    public Set<BoardLine> getLines() {
        Set<BoardLine> set = new HashSet<>();
        set.add(new BoardLine(this, "§1", 11));

        stage = new BoardLine(this, "Стадия: §a", 10);
        set.add(stage);
        dynamicLine(stage.getNumber(), GameFactory.getStageSwitcher().getCurrentStage().getName() + " " + GameFactory.getStageSwitcher().getTimeToEnd());

        set.add(new BoardLine(this, "§4", 9));

        if (Gamer.getGamer(player).getPerk() == null) {
            set.add(new BoardLine(this, "§fСпособность: §cN/A", 8));
        }else{
            set.add(new BoardLine(this, "§fСпособность: §a"+Gamer.getGamer(player).getPerk().getName(), 8));
        }

        set.add(new BoardLine(this, "§5", 7));

        players = new BoardLine(this, "Игроков: §a", 6);
        set.add(players);
        dynamicLine(players.getNumber(), "Игроков: §a", +GameFactory.players.size() + "");
        spectators = new BoardLine(this, "Наблюдает: §a", 5);
        set.add(spectators);
        int spectator = Bukkit.getOnlinePlayers().size() - GameFactory.getPlayers();
        dynamicLine(players.getNumber(), "Наблюдателей: §a", +spectator + "");
        set.add(new BoardLine(this, "§3", 4));
        kills = new BoardLine(this, "Убийств: §a", 3);
        set.add(kills);
        dynamicLine(players.getNumber(), "Убийств: §a", +Gamer.getGamer(player).getKills() + "");
        update(() -> {
            dynamicLine(kills.getNumber(), "Убийств: §a", "" + Gamer.getGamer(player).getKills());
            dynamicLine(players.getNumber(), "Игроков: §a", "" + Utils.getAlivePlayers().size());
            int spectator1 = Bukkit.getOnlinePlayers().size() - Utils.getAlivePlayers().size();
            dynamicLine(spectators.getNumber(), "Наблюдателей: §a", "" + spectator1);
        }, 100L);
        update(() -> dynamicLine(stage.getNumber(), GameFactory.getStageSwitcher().getCurrentStage().getName() + " " + GameFactory.getStageSwitcher().getTimeToEnd()), 20L);
        return set;
    }

}
