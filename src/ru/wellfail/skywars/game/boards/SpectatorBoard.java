package ru.wellfail.skywars.game.boards;

import org.bukkit.Bukkit;
import ru.wellfail.skywars.Main;
import ru.wellfail.skywars.game.GameFactory;
import ru.wellfail.wapi.board.Board;
import ru.wellfail.wapi.board.BoardLine;
import ru.wellfail.wapi.game.GameSettings;
import ru.wellfail.wapi.utils.Utils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
/**
 * Created by WellFail on 02.06.2015.
 */
public class SpectatorBoard extends Board {

    private int d;

    public SpectatorBoard() {
        Set<BoardLine> lines = getLines();
        list = Main.getInstance().getBoard();
        create(lines, list.get(0));
        update(() -> {
            if (d == list.size())
                d = 0;
            setDisplay(list.get(d));
            d++;
        }, 3L);
        startUpdate();
    }

    private List<String> list;
    private BoardLine  players, spectators;

    public Set<BoardLine> getLines() {
        Set<BoardLine> set = new HashSet<>();
        set.add(new BoardLine(this, "§1", 6));
        players = new BoardLine(this, "Игроков: §a", 5);
        set.add(players);
        dynamicLine(players.getNumber(), "Игроков: §a", + GameFactory.players.size() + "");
        spectators = new BoardLine(this, "Наблюдает: §a", 4);
        set.add(spectators);
        int spectator = Bukkit.getOnlinePlayers().size() - GameFactory.getPlayers();
        dynamicLine(players.getNumber(), "Наблюдателей: §a", +spectator + "");
        set.add(new BoardLine(this, "§3", 3));
        set.add(new BoardLine(this,"Карта: §a"+ GameSettings.mapLocation.getWorld().getName(),2));
        set.add(new BoardLine(this,"Сервер: §a"+ ru.wellfail.wapi.Main.getUsername(),1));

        update(() -> {
            dynamicLine(players.getNumber(), "Игроков: §a", "" + Utils.getAlivePlayers().size());
            int spectator1 = Bukkit.getOnlinePlayers().size() - Utils.getAlivePlayers().size();
            dynamicLine(spectators.getNumber(), "Наблюдателей: §a", "" + spectator1);

        }, 100L);
        return set;
    }

}
