package ru.wellfail.skywars.data.shop;

/**
 * Created by WellFail on 20.02.2016.
 */
public enum SouldGrade {
    KILLER(11), COLLECTOR(12);

    private int id;

    public int getId() {
        return this.id;
    }

    SouldGrade(int id) {
        this.id = id;
    }

    public static SouldGrade getType(int id){
        for(SouldGrade type:values()){
            if(type.getId() == id){
                return type;
            }
        }
        return null;
    }
}
