package ru.wellfail.skywars.data.shop;

import org.bukkit.event.EventHandler;
import ru.wellfail.shop.ShopManager;
import ru.wellfail.shop.data.Purchasable;
import ru.wellfail.skywars.data.GPlayer;
import ru.wellfail.skywars.game.GameFactory;
import ru.wellfail.wapi.events.LoadShopEvent;
import ru.wellfail.wapi.game.Gamer;
import ru.wellfail.wapi.listeners.FListener;
import ru.wellfail.wapi.perks.PerkType;

/**
 * Created by WellFail on 18.02.2016.
 */
public class ShopLoader extends FListener {

    @EventHandler
    public void onLoad(LoadShopEvent event) {

        Gamer gamer = event.getGamer();

        if (!GameFactory.players.containsKey(gamer.getPlayer().getName())) {
            GameFactory.players.put(gamer.getPlayer().getName(), new GPlayer(gamer.getPlayer()));
        }
        GPlayer player = GameFactory.players.get(gamer.getPlayer().getName());


        Purchasable item = ShopManager.purchases.get(event.getId());


        if (item == null) {
            return;
        }
        if (item.getType() == Purchasable.PurchaseType.CAGE) {
            player.setCage(item.getItem());

        }
        if (item.getType() == Purchasable.PurchaseType.SOULDUPGRADE) {
            player.setSoulGrade(SouldGrade.getType(event.getId()));
        }

        GameFactory.players.put(gamer.getPlayer().getName(), player);


    }
}
