package ru.wellfail.skywars.data;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.wellfail.skywars.Main;
import ru.wellfail.skywars.game.GameFactory;
import ru.wellfail.skywars.game.boards.SpectatorBoard;
import ru.wellfail.wapi.board.Board;
import ru.wellfail.wapi.game.Game;
import ru.wellfail.wapi.game.GameSettings;
import ru.wellfail.wapi.game.Gamer;
import ru.wellfail.wapi.gui.Item;
import ru.wellfail.wapi.gui.ItemRunnable;
import ru.wellfail.wapi.shop.Kit;
import ru.wellfail.wapi.shop.ShopManager;
import ru.wellfail.wapi.utils.FIterator;
import ru.wellfail.wapi.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by WellFail on 12.02.2016.
 */
public class Config {

    private FIterator<Location> spawns;
    public int delayColb;

    private Main instance;

    public FIterator<Location> getSpawns(){
        return this.spawns;
    }

    public int getDelayColb(){
        return this.delayColb;
    }

    public Config(){
        instance = Main.getInstance();
        init();
        loadConfig();
        loadItems();
    }
    private static FileConfiguration configShop;
    private File locationsFile = null;

    public void saveLocationsConfig() {
        if ((locationsFile == null) || (configShop == null))
            return;
        try {
            configShop.save(locationsFile);
        } catch (IOException ex) {
            System.out.println("Ошибка при чтении файла конфигурации.");
        }
    }

    private void init() {
        locationsFile = new File(Main.getInstance().getDataFolder(), "kits.yml");
        configShop = YamlConfiguration.loadConfiguration(locationsFile);
        InputStream defConfigStream1 = Main.getInstance().getResource("kits.yml");
        if (defConfigStream1 != null) {
            YamlConfiguration defConfig1 = YamlConfiguration.loadConfiguration(defConfigStream1);
            configShop.setDefaults(defConfig1);
        }
        configShop.options().copyDefaults(true);
        saveLocationsConfig();
    }

    private void loadConfig() {
        try {
            FileConfiguration config = instance.getConfig();
            delayColb = config.getInt("DStart");
            GameSettings.spectrLocation = Utils.stringToLocation(config.getString("Spectator"));
            for (String s : configShop.getConfigurationSection("Shop").getKeys(false)) {
                ShopManager.instance.addKit(new Kit(configShop.getString("Shop." + s + ".kit"), configShop.getStringList("Shop." + s + ".lore"), configShop.getInt("Shop." + s + ".perm")));
            }
            GameSettings.respawnLocation = Utils.stringToLocation(config.getString("Lobby"));
            Set<Location> set = config.getStringList("Spawns").stream().map(Utils::stringToLocation).collect(Collectors.toCollection(LinkedHashSet::new));
            GameSettings.mapLocation = Utils.stringToLocation(config.getString("Map"));
            this.spawns = new FIterator(set);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        Board.spectator = new SpectatorBoard();

    }

    private void loadItems() {
        ItemStack shop = new ItemStack(Material.CHEST);
        ItemMeta meta = shop.getItemMeta();
        meta.setDisplayName("§aМагазин наборов §7(ПКМ)");
        shop.setItemMeta(meta);
        Game.getInstance().shopItem = new Item(shop, new ItemRunnable() {
            public void onUse(PlayerInteractEvent e) {
                Gamer gamer = Gamer.getGamer(e.getPlayer());
                if (gamer.getPurchase() != null && !gamer.isCancelbuy()) {
                    e.getPlayer().sendMessage(GameSettings.prefix + "Нажмите еще раз, чтобы аннулировать покупку набора " + gamer.getPurchase().getName());
                    e.setCancelled(true);
                    e.getPlayer().closeInventory();
                    gamer.setCancelbuy(true);
                    return;
                }
                if (gamer.getPurchase() != null) {
                    e.getPlayer().sendMessage(GameSettings.prefix + "Вам были возвращены деньги за покупку набора " + gamer.getPurchase().getName());
                    ru.wellfail.wapi.Main.economy.depositPlayer(e.getPlayer().getName(), gamer.getPurchase().getPrice());
                    gamer.setCancelbuy(false);
                    gamer.setPurchase(null);
                    e.setCancelled(true);
                    e.getPlayer().closeInventory();
                    return;
                }

                Inventory inv = Bukkit.createInventory(null, 45, "§rМагазин наборов");
                for (int i = 0; i < 45; i++) {
                    Kit kit = ShopManager.instance.getKit(i);
                    if (kit == null) {
                        continue;
                    }
                    inv.setItem(i, kit.getItem());
                }
                e.getPlayer().openInventory(inv);
            }

            public void onClick(InventoryClickEvent paramInventoryClickEvent) {
            }
        }, new Action[]{Action.RIGHT_CLICK_AIR, Action.RIGHT_CLICK_BLOCK});
    }
}
