package ru.wellfail.skywars.data;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import ru.wellfail.skywars.data.shop.SouldGrade;
import ru.wellfail.wapi.game.Gamer;

/**
 * Created by WellFail on 02.06.2015.
 */
public class GPlayer {

    private Player player;

    private ItemStack cage;
    private SouldGrade soulGrade;

    public SouldGrade getSoulGrade(){return this.soulGrade;}

    public ItemStack getCage() {
        return this.cage;
    }

    public void setSoulGrade(SouldGrade grade){
        this.soulGrade = grade;
    }

    public void setCage(ItemStack cage) {
        this.cage = cage;
    }

    public GPlayer(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return this.player;
    }


    public int getFb() {
        return Gamer.getGamer(player).getFb();
    }

}
