package ru.wellfail.skywars.data;

import org.bukkit.entity.Player;
import ru.wellfail.skywars.game.GameFactory;
import ru.wellfail.wapi.game.Game;
import ru.wellfail.wapi.game.Gamer;
import ru.wellfail.wapi.logger.WLogger;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;

/**
 * Created by WellFail on 12.02.2016.
 */
public class Stats {


    private final String create = "ALTER TABLE stats ADD swfb INT DEFAULT 0, ADD swdeaths INT DEFAULT 0,ADD swwins INT DEFAULT 0, ADD swgames INT DEFAULT 0, ADD swkills INT DEFAULT 0, ADD souls INT DEFAULT 0;";

    public Stats() {
        Game.createcolumns(create);
    }

    public void sava(Collection<Gamer> gamers, Player lastAlive) {
        Map<String, GPlayer> players = GameFactory.players;

        for (Gamer gamer : gamers) {
            if (players == null || gamer == null || gamer.getPlayer() == null || players.get(gamer.getPlayer().getName()) == null) {
                continue;
            }
            String sql;
            boolean win = false;
            if (lastAlive != null && lastAlive.getName().equals(gamer.getPlayer().getName())) {
                win = true;
            }
            int souls = gamer.getSouls();
            sql = "INSERT INTO `stats` (`name`, `swfb`, `swgames`, `swkills`, `swwins`, `swdeaths`,`souls`) " + "VALUES ('" + gamer.getPlayer().getName() + "', '" + players.get(gamer.getPlayer().getName()).getFb() + "', '1'," + "'" + gamer.getKills() + "'," + "'" + (win ? 1 : 0) + "'," + "'" + (!win ? 1 : 0) + "','"+souls+"')" + "ON DUPLICATE KEY UPDATE ";

            if (gamer.getKills() != 0)
                sql += "`swkills`=`swkills`+'" + gamer.getKills() + "',";
            if (!win)
                sql += "`swdeaths`=`swdeaths`+'" + 1 + "',";
            if (win)
                sql += "`swwins`=`swwins`+'1',";
            sql += "`swfb`=`swfb`+'" + players.get(gamer.getPlayer().getName()).getFb() + "',";
            sql += "`swgames`=`swgames`+'1',`souls`= '"+gamer.getSouls()+"'";

            try {
                WLogger.debug(sql);
                Game.saveStat(sql);
            } catch (SQLException e) {
                e.printStackTrace();
            }


        }
    }

}
